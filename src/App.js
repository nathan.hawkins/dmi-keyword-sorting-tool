import React, { useState } from 'react';
import './App.scss';
import Papa from 'papaparse';
import { read, utils } from 'xlsx';
import { useDropzone } from 'react-dropzone';
import { FaFileExport } from "react-icons/fa6";

const CSVGroupExportApp = () => {
    const [file, setFile] = useState(null);
    const [groups, setGroups] = useState([{ type: 'Custom Text', matchType: 'Contains', keyword: '' }]);
    const [uploadedFileName, setUploadedFileName] = useState('');
    const [autoGroup, setAutoGroup] = useState(true); // Checkbox is checked by default

    const handleGroupChange = (index, key, value) => {
        const newGroups = [...groups];
        newGroups[index] = { ...newGroups[index], [key]: value };
        setGroups(newGroups);
    };

    const addGroup = () => {
        setGroups([...groups, { type: 'Custom Text', matchType: 'Contains', keyword: '' }]);
    };

    const matchesWholeWord = (cell, word) => {
        const regex = new RegExp(`\\b${word}\\b`, 'i');
        return regex.test(cell);
    };

    const processAndDownloadCSV = () => {
        if (!file) {
            alert('Please upload a file.');
            return;
        }

        const reader = new FileReader();
        reader.onload = (e) => {
            const data = e.target.result;
            let parsedData;

            if (file.type.includes('sheet')) {
                const workbook = read(data, { type: 'binary' });
                const worksheet = workbook.Sheets[workbook.SheetNames[0]];
                parsedData = utils.sheet_to_json(worksheet, { header: 1 });
            } else {
                parsedData = Papa.parse(data, { header: false }).data;
            }

            const headerRow = parsedData[0];
            let dataRows = parsedData.slice(1);

            let processedData = [];

            if (autoGroup) {
                const wordGroups = {};
                dataRows.forEach(row => {
                    const words = row[0].toString().split(/\s+/);
                    words.forEach(word => {
                        if (word) {
                            word = word.toLowerCase();
                            if (!wordGroups[word]) {
                                wordGroups[word] = [];
                            }
                            wordGroups[word].push(row);
                        }
                    });
                });

                Object.keys(wordGroups).forEach(word => {
                    processedData.push([`Keyword match: ${word}`]);
                    wordGroups[word].forEach(row => processedData.push(row));
                    processedData.push(['']);
                });
            } else {

                let groupedRows = new Set();

                groups.forEach((group, groupIndex) => {
                    if (group.keyword) {
                        processedData.push([`Keyword match: ${group.keyword}`]);
                        dataRows.forEach((row, rowIndex) => {
                            if ((group.matchType === 'Exact Match' && matchesWholeWord(row[0].toString(), group.keyword)) ||
                                (group.matchType === 'Contains' && row[0].toString().toLowerCase().includes(group.keyword.toLowerCase()))) {
                                processedData.push(row);
                                groupedRows.add(rowIndex);
                            }
                        });
                    }

                    processedData.push(['']);
                });

                // ungrouped data
                if (dataRows.some((_, rowIndex) => !groupedRows.has(rowIndex))) {
                    processedData.push(['Ungrouped']);
                    dataRows.forEach((row, rowIndex) => {
                        if (!groupedRows.has(rowIndex)) {
                            processedData.push(row);
                        }
                    });
                }
            }

            // Prepend header row and convert to CSV
            processedData.unshift(headerRow);

            const newCsv = Papa.unparse(processedData);
            const blob = new Blob([newCsv], { type: 'text/csv;charset=utf-8;' });
            const link = document.createElement('a');
            link.href = URL.createObjectURL(blob);
            link.download = 'processed_data.csv';
            link.click();
        };

        if (file.type.includes('csv')) {
            reader.readAsText(file);
        } else {
            reader.readAsBinaryString(file);
        }
    };

    const { getRootProps, getInputProps } = useDropzone({
        accept: '.csv, application/vnd.openxmlformats-officedocument.spreadsheetml.sheet, application/vnd.ms-excel',
        onDrop: (acceptedFiles) => {
            setFile(acceptedFiles[0]);
            setUploadedFileName(acceptedFiles[0].name);
        }
    });

    return (
        <div>
            <a className="navbar-brand" href="https://www.dmipartners.com">
                <span className="symbol"></span><span className="copyline"></span>
                <span>DMi Partners Affiliate Newsletter Tool</span>
            </a>
            <div className="container">
                <div className="row">
                    <div className="header-container">
                        <h1>Keyword Grouping Tool</h1>
                        <div className="auto-switch-container">
                            <h2>Auto-group</h2>
                            <div className="auto-switch">
                                <input
                                    className="tgl tgl-skewed"
                                    id="cb3"
                                    type="checkbox"
                                    checked={autoGroup}
                                    onChange={(e) => setAutoGroup(e.target.checked)}
                                />
                                <label
                                    className="tgl-btn"
                                    data-tg-off="OFF"
                                    data-tg-on="ON"
                                    htmlFor="cb3"
                                ></label>
                            </div>
                        </div>
                    </div>
                    <p>This tool will search and group keywords from the first column of a spreadsheet together automatically. To use this tool, first upload a file of an accepted format (.csv, .xls, .xlsx). To have the keywords grouped automatically, all you have to do after your file is uploaded is click the export button in the bottom right-hand corner. Auto-grouping is enabled by default. If you are wanting to search for specific terms, turn Automatic Grouping off. This will show you your custom text group tools. You can add as many keyword groups as you'd like by clicking the '+ New Group' button at the bottom. When you're ready, click the export button. Your completed CSV will automatically begin downloading.</p>
                </div>
                <div className="csv-group-export-app">
                    <div className="row">
                        <label htmlFor="folderName">Upload file</label>
                        <div {...getRootProps({ className: `dropzone ${uploadedFileName ? 'filePresent' : ''}` })}>
                            <input {...getInputProps()} />
                            {uploadedFileName ? <p>File uploaded: {uploadedFileName}</p> : <p>Drag 'n' drop a file here, or click to select files</p>}
                        </div>
                    </div>

                    {!autoGroup && groups.map((group, index) => (
                        <div className="row keywordGroup" key={index}>
                            <div className="left">
                                <label>Group Type</label>
                                <select value={group.type} onChange={(e) => handleGroupChange(index, 'type', e.target.value)}>
                                    <option value="Custom Text">Custom Text</option>
                                </select>
                                <div>
                                    <label>Match Type</label>
                                    <select value={group.matchType} onChange={(e) => handleGroupChange(index, 'matchType', e.target.value)}>
                                        <option value="Contains">Contains</option>
                                        <option value="Exact Match">Exact Match</option>
                                    </select>
                                </div>
                            </div>

                            <div className="right">
                                <label>Keyword</label>
                                <input
                                    type="text"
                                    value={group.keyword}
                                    onChange={(e) => handleGroupChange(index, 'keyword', e.target.value)}
                                />
                                <p>"Contains" will search the uploaded document and group keywords where the input occurs anywhere in the cell. "Exact Match" will group cells based on if the input matches exactly.</p>
                            </div>
                        </div>
                    ))}

                    {!autoGroup && (
                        <button onClick={addGroup}>+ New Group</button>
                    )}
                    <div className="floatingButton">
                        <button onClick={processAndDownloadCSV}>
                            <span className="plus"><FaFileExport /></span>
                            <span className="newGroup">Export</span>
                        </button>
                    </div>
                </div>
            </div>
        </div>
    );
};

export default CSVGroupExportApp;
